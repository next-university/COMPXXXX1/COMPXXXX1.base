DESCRIPTION = "Dummy recipe for tinyalsa"
LICENSE = "BSD"

SRC_URI = "git://github.com/tinyalsa/tinyalsa;protocol=https"
SRCREV = "ea9da6e1b16c14408b3bc5323cdc7560366bb943"

S = "${WORKDIR}/git"

inherit cmake
